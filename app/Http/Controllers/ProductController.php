<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function show(Product $product){
        $product->load('product_images');
        return view('products.show')->with(compact('product'));
    }
}
