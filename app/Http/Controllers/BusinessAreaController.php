<?php

namespace App\Http\Controllers;

use App\BusinessArea;
use Illuminate\Http\Request;

class BusinessAreaController extends Controller
{
    public function show(BusinessArea $business_area){
        return view('business_areas.show')->with(compact('business_area'));
    }
}
