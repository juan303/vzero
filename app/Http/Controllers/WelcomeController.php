<?php

namespace App\Http\Controllers;

use App\BusinessArea;
use App\FamilyProduct;
use App\News;
use App\Service;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index(){
        $business_areas = BusinessArea::active(true)->get();
        $family_products = FamilyProduct::active(true)->get();
        $news = News::active(true)->get();
        $services = Service::active(true)->get();
        return view('welcome')->with(compact('business_areas', 'family_products', 'news', 'services'));
    }
}
