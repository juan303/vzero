<?php

namespace App\Http\Controllers;

use App\FamilyProduct;
use Illuminate\Http\Request;

class FamilyProductController extends Controller
{
    public function index(FamilyProduct $family_product){
        $family_product->load('products');
        return view('family_products.index')->with(compact('family_product'));
    }
}
