<?php

namespace App\Http\Controllers\Admin;

use App\BusinessArea;
use App\BusinessAreasTranslation;
use App\Http\Requests\BusinessAreaRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class BusinessAreaController extends Controller
{
    public function index(){
        $business_areas = BusinessArea::all();
        /*$business_areas = BusinessArea::with(['business_areas_translation' => function($q){
            $q->where('locale', session()->get('locale'));
        }])->get();*/
        return view('admin.business_areas.index')->with(compact('business_areas'));
    }

    public function create(){
        return view('admin.business_areas.create');
    }

    public function store(BusinessAreaRequest $request){

        $success = true;
        DB::beginTransaction();
        try{
            $visible = true;
            if(!$request->has('visible')){
                $visible = false;
            }

            $new_business_area = BusinessArea::create([
                'active' => $visible,
                'name' => [
                    'es' => $request->input('name_es'),
                    'en' => $request->input('name_en')
                ],
                'description' => [
                    'es' => $request->input('description_es'),
                    'en' => $request->input('description_en')
                ],
            ]);
            $fileName = null;
            if($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = uniqid() . $file->getClientOriginalName();
                if($imagePath = $file->storeAs('images', $fileName)){
                    $intervention = new ImageManager(['driver'=>'gd']);
                    $image = $intervention->make(Storage::get($imagePath))->fit(640, 480)->encode();
                    Storage::disk('public')->put('images/business_areas/'.$new_business_area->id."/featured/".$fileName, $image);
                    Storage::disk('public')->delete('images/'.$fileName);
                }
            }

            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_es'));
            $images_es = $dom->getElementsByTagName('img');
            $short_text_es = $dom->getElementsByTagName('p')->item(0)->textContent;

            foreach($images_es as $k => $image){
                $data = $image->getAttribute('src');
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                $imageFileName = uniqid().$k.'.jpg';
                Storage::disk('public')->put('images/business_areas/'.$new_business_area->id.'/text_images/'.$imageFileName, $data);
                $image->removeAttribute('src');
                $image->setAttribute('src', asset('storage/images/business_areas/'.$new_business_area->id.'/text_images/'.$imageFileName));

            }
            $text_es = $dom->saveHTML();

            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_en'));
            $images_en = $dom->getElementsByTagName('img');
            $short_text_en = $dom->getElementsByTagName('p')->item(0)->textContent;


            foreach($images_en as $k => $image){
                $data = $image->getAttribute('src');
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                $imageFileName = uniqid().$k.'.jpg';
                Storage::disk('public')->put('images/business_areas/'.$new_business_area->id.'/text_images/'.$imageFileName, $data);
                $image->removeAttribute('src');
                $image->setAttribute('src', asset('storage/images/business_areas/'.$new_business_area->id.'/text_images/'.$imageFileName));
            }
            $text_en = $dom->saveHTML();

            $new_business_area->update([
                'image' => $fileName,//esto solo lo hago con la version en español
                'text' => [
                    'es' => $text_es,
                    'en' => $text_en
                ],
                'short_text' => [
                    'es' => $short_text_es,
                    'en' => $short_text_en
                ]
            ]);
        } catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            session()->flash('message', ['type' => 'success', 'text'=>'Area de negocio registrada correctamente']);
            return redirect(route('business_area.index'));
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
            return back();
        }

    }

    public function edit(BusinessArea $business_area){
        return view('admin.business_areas.edit')->with(compact('business_area'));
    }

    public function update(BusinessAreaRequest $request, BusinessArea $business_area){

        $success = true;
        DB::beginTransaction();
        try{
            $visible = true;
            if(!$request->has('visible')){
                $visible = false;
            }

            if($request->hasFile('image')) {
                if($business_area->image != null) {
                    Storage::disk('public')->delete('images/business_areas/' . $business_area->id . "/featured/" . $business_area->image);
                }
                $file = $request->file('image');
                $fileName = uniqid() . $file->getClientOriginalName();
                if($imagePath = $file->storeAs('images', $fileName)){
                    $intervention = new ImageManager(['driver'=>'gd']);
                    $image = $intervention->make(Storage::get($imagePath))->fit(640, 480)->encode();
                    Storage::disk('public')->put('images/business_areas/'.$business_area->id."/featured/".$fileName, $image);
                    Storage::disk('public')->delete('images/'.$fileName);

                    $business_area->update([
                        'image' => $fileName
                    ]);
                }
            }

            /*===================================================================================================UPDATE ES===*/
            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_es'));
            $images_es = $dom->getElementsByTagName('img');
            $short_text_es = $dom->getElementsByTagName('p')->item(0)->textContent;
            $iteration = 0;
            foreach($images_es as $k => $image){
                $data = $image->getAttribute('src');
                $image_path = explode('/', $data);
                $name_of_file = array_pop($image_path);

                if(!Storage::exists('images/business_areas/'.$business_area->id.'/text_images/'.$name_of_file)){
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);
                    $ImageFileName = uniqid().$k.'.jpg';
                    Storage::disk('public')->put('images/business_areas/'.$business_area->id.'/text_images/'.$ImageFileName, $data);
                    $image->removeAttribute('src');
                    $image->setAttribute('src', asset('storage/images/business_areas/'.$business_area->id.'/text_images/'.$ImageFileName));
                }
            }
            $text_es = $dom->saveHTML();
            /*===================================================================================================END UPDATE ES===*/

            /*===================================================================================================UPDATE EN===*/
            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_en'));
            $images_en = $dom->getElementsByTagName('img');
            $short_text_en = $dom->getElementsByTagName('p')->item(0)->textContent;

            foreach($images_en as $k => $image){
                $data = $image->getAttribute('src');
                $image_path = explode('/', $data);
                $name_of_file = array_pop($image_path);

                if(!Storage::exists('images/business_areas/'.$business_area->id.'/text_images/'.$name_of_file)){
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);
                    $ImageFileName = uniqid().$k.'.jpg';
                    Storage::disk('public')->put('images/business_areas/'.$business_area->id.'/text_images/'.$ImageFileName, $data);
                    $image->removeAttribute('src');
                    $image->setAttribute('src', asset('storage/images/business_areas/'.$business_area->id.'/text_images/'.$ImageFileName));
                }
            }
            $text_en = $dom->saveHTML();
            /*===============================================================================================END UPDATE EN======*/

            $business_area->update([
                'active' => $visible,
                'name' => [
                    'es' => $request->input('name_es'),
                    'en' => $request->input('name_en')
                ],
                'description' => [
                    'es' => $request->input('description_es'),
                    'en' => $request->input('description_en')
                ],
                'text' => [
                    'es' => $text_es,
                    'en' => $text_en
                ],
                'short_text' => [
                    'es' => $short_text_es,
                    'en' => $short_text_en
                ]
            ]);


        } catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            session()->flash('message', ['type' => 'success', 'text'=>'Noticia actualizada correctamente']);
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
        }
        return back();

    }


    public function destroy(BusinessArea $business_area){
        $success = true;

        DB::beginTransaction();
        try{
            $business_area->delete();
        } catch(\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            //return response()->json(['type' => 'success', 'text' => 'Producto #'.$product->id.' eliminado correctamente']);
            session()->flash('message', ['type' => 'success', 'text'=>'Área de negocio eliminada correctamente']);
        }
        else{
            //return response()->json(['type' => 'warning', 'text' => $success]);
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
        }
        return back();
    }
}
