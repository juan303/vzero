<?php

namespace App\Http\Controllers\Admin;

use App\BusinessArea;
use App\FamilyProduct;
use App\Http\Requests\ProductRequest;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{

    public function index(){
        $products = Product::all();
        /*$business_areas = BusinessArea::with(['business_areas_translation' => function($q){
            $q->where('locale', session()->get('locale'));
        }])->get();*/
        return view('admin.products.index')->with(compact('products'));
    }

    public function create(){
        $business_areas = BusinessArea::all();
        $family_products = FamilyProduct::all();
        return view('admin.products.create')->with(compact('business_areas', 'family_products'));
    }

    public function store(ProductRequest $request){

        $success = true;
        DB::beginTransaction();
        try{
            $visible = true;
            if(!$request->has('visible')){
                $visible = false;
            }


            $product = Product::create([
                'active' => $visible,
                'name' => [
                    'es' => $request->input('name_es'),
                    'en' => $request->input('name_en')
                ],
                'description' => [
                    'es' => $request->input('description_es'),
                    'en' => $request->input('description_en')
                ],
                'family_product_id' => $request->input('family_product')
            ]);

            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_es'));
            $images_es = $dom->getElementsByTagName('img');

            foreach($images_es as $k => $image){
                $data = $image->getAttribute('src');
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                $ImageFileName = uniqid().$k.'.jpg';
                Storage::disk('public')->put('images/products/'.$product->id.'/text_images/'.$ImageFileName, $data);
                $image->removeAttribute('src');
                $image->setAttribute('src', asset('storage/images/products/'.$product->id.'/text_images/'.$ImageFileName));
            }
            $text_es = $dom->saveHTML();

            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_en'));
            $images_en = $dom->getElementsByTagName('img');

            foreach($images_en as $k => $image){
                $data = $image->getAttribute('src');
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                $ImageFileName = uniqid().$k.'.jpg';
                Storage::disk('public')->put('images/products/'.$product->id.'/text_images/'.$ImageFileName, $data);
                $image->removeAttribute('src');
                $image->setAttribute('src', asset('storage/images/products/'.$product->id.'/text_images/'.$ImageFileName));
            }
            $text_en = $dom->saveHTML();

            $product->update([
                'text' => [
                    'es' => $text_es,
                    'en' => $text_en
                ]
            ]);
            if($request->input('business_areas')) {
                foreach ($request->input('business_areas') as $business_area_id) {
                    $product->business_areas()->attach($business_area_id);
                }
            }
        } catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true) {
            DB::commit();
            session()->flash('message', ['type' => 'success', 'text' => 'Producto registrado correctamente']);
            return redirect(route('product_images.index', ['product' => $product->id]));
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
            return back();
        }

    }

    public function edit(Product $product){
        $product->load('family_product', 'business_areas');
        $family_products = FamilyProduct::all();
        $business_areas = BusinessArea::all();
        return view('admin.products.edit')->with(compact('product', 'family_products', 'business_areas'));
    }

    public function update(ProductRequest $request, Product $product){
        $success = true;
        DB::beginTransaction();
        try{
            $visible = true;
            if(!$request->has('visible')){
                $visible = false;
            }

            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_es'));
            $images_es = $dom->getElementsByTagName('img');

            foreach($images_es as $k => $image) {
                $data = $image->getAttribute('src');
                $image_path = explode('/', $data);
                $name_of_file = array_pop($image_path);

                if(!Storage::exists('images/products/'.$product->id.'/text_images/'.$name_of_file)) {
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);

                    $ImageFileName = uniqid() . $k . '.jpg';
                    Storage::disk('public')->put('images/products/' . $product->id . '/text_images/' . $ImageFileName, $data);
                    $image->removeAttribute('src');
                    $image->setAttribute('src', asset('storage/images/products/' . $product->id . '/text_images/' . $ImageFileName));
                }
            }
            $text_es = $dom->saveHTML();

            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_en'));
            $images_en = $dom->getElementsByTagName('img');


            foreach($images_en as $k => $image) {
                $data = $image->getAttribute('src');
                $image_path = explode('/', $data);
                $name_of_file = array_pop($image_path);
                if(!Storage::exists('images/products/'.$product->id.'/text_images/'.$name_of_file)) {
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);

                    $ImageFileName = uniqid() . $k . '.jpg';
                    Storage::disk('public')->put('images/products/' . $product->id . '/text_images/' . $ImageFileName, $data);
                    $image->removeAttribute('src');
                    $image->setAttribute('src', asset('storage/images/products/' . $product->id . '/text_images/' . $ImageFileName));
                }
            }
            $text_en = $dom->saveHTML();

            $product->update ([
                'active' => $visible,
                'name' => [
                    'es' => $request->input('name_es'),
                    'en' => $request->input('name_en')
                ],
                'description' => [
                    'es' => $request->input('description_es'),
                    'en' => $request->input('description_en')
                ],
                'text' => [
                    'es' => $text_es,
                    'en' => $text_en
                ],
                'family_product_id' => $request->input('family_product')
            ]);

            foreach($product->business_areas as $business_area){
                $product->business_areas()->detach($business_area->id);
            }
            if($request->input('business_areas')) {
                foreach ($request->input('business_areas') as $business_area_id) {
                    $product->business_areas()->attach($business_area_id);
                }
            }

        } catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true) {
            DB::commit();
            session()->flash('message', ['type' => 'success', 'text' => 'Producto editado correctamente']);
            //return redirect(route('product_image.index', ['product' => $product->id]));
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
        }
        return back();

    }


    public function destroy(Product $product){
        $success = true;

        DB::beginTransaction();
        try{
            $product->delete();
        } catch(\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            //return response()->json(['type' => 'success', 'text' => 'Producto #'.$product->id.' eliminado correctamente']);
            session()->flash('message', ['type' => 'success', 'text'=>'Producto eliminado correctamente']);
        }
        else{
            //return response()->json(['type' => 'warning', 'text' => $success]);
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
        }
        return back();
    }

}
