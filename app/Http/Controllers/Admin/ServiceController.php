<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ServiceRequest;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ServiceController extends Controller
{
    public function index(){
        $services = Service::all();
        return view('admin.services.index')->with(compact('services'));
    }

    public function create(){
        return view('admin.services.create');
    }

    public function store(ServiceRequest $request){

        $success = true;
        DB::beginTransaction();
        try{
            $visible = true;
            if(!$request->has('visible')){
                $visible = false;
            }
            $new_service = Service::create([
                'active' => $visible,
                'name' => [
                    'es' => $request->input('name_es'),
                    'en' => $request->input('name_en')
                ],
                'description' => [
                    'es' => $request->input('description_es'),
                    'en' => $request->input('description_en')
                ],
            ]);

            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_es'));
            $images_es = $dom->getElementsByTagName('img');
            $short_text_es = $dom->getElementsByTagName('p')->item(0)->textContent;

            $iteration = 0;//esto solo lo hago con la version en español
            $featured_image = null;//esto solo lo hago con la version en español
            foreach($images_es as $k => $image){
                $data = $image->getAttribute('src');
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                $fileName = uniqid().$k.'.jpg';
                Storage::disk('public')->put('images/services/'.$new_service->id.'/text_images/'.$fileName, $data);
                $image->removeAttribute('src');
                $image->setAttribute('src', asset('storage/images/services/'.$new_service->id.'/text_images/'.$fileName));
                $featured_image = $fileName; //esto solo lo hago con la version en español
                $iteration++;//esto solo lo hago con la version en español
            }
            $text_es = $dom->saveHTML();


            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_en'));
            $images_en = $dom->getElementsByTagName('img');
            $short_text_en = $dom->getElementsByTagName('p')->item(0)->textContent;


            foreach($images_en as $k => $image){
                $data = $image->getAttribute('src');
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                $fileName = uniqid().$k.'.jpg';
                Storage::disk('public')->put('images/services/'.$new_service->id.'/text_images/'.$fileName, $data);
                $image->removeAttribute('src');
                $image->setAttribute('src', asset('storage/images/services/'.$new_service->id.'/text_images/'.$fileName));
            }
            $text_en = $dom->saveHTML();

            $new_service->update([
                'image' => $featured_image,//esto solo lo hago con la version en español
                'text' => [
                    'es' => $text_es,
                    'en' => $text_en
                ],
                'short_text' => [
                    'es' => $short_text_es,
                    'en' => $short_text_en
                ]
            ]);
        } catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            session()->flash('message', ['type' => 'success', 'text'=>'Servicio registrado correctamente']);
            return redirect(route('service.index'));
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
            return back();
        }

    }

    public function edit(Service $service){
        return view('admin.services.edit')->with(compact('service'));
    }

    public function update(ServiceRequest $request, Service $service){
        $success = true;
        DB::beginTransaction();
        try{
            $visible = true;
            if(!$request->has('visible')){
                $visible = false;
            }

            /*===================================================================================================UPDATE ES===*/
            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_es'));
            $images_es = $dom->getElementsByTagName('img');
            $short_text_es = $dom->getElementsByTagName('p')->item(0)->textContent;
            $iteration = 0;
            foreach($images_es as $k => $image){
                $data = $image->getAttribute('src');
                $image_path = explode('/', $data);
                $name_of_file = array_pop($image_path);

                if(!Storage::exists('images/services/'.$service->id.'/text_images/'.$name_of_file)){
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);
                    $fileName = uniqid().$k.'.jpg';
                    Storage::disk('public')->put('images/services/'.$service->id.'/text_images/'.$fileName, $data);
                    $image->removeAttribute('src');
                    $image->setAttribute('src', asset('storage/images/services/'.$service->id.'/text_images/'.$fileName));
                    if($iteration == 0){
                        $service->update([
                            'image' => $fileName
                        ]);
                    }
                }
                $iteration++;
            }
            $text_es = $dom->saveHTML();
            /*===================================================================================================END UPDATE ES===*/

            /*===================================================================================================UPDATE EN===*/
            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_en'));
            $images_en = $dom->getElementsByTagName('img');
            $short_text_en = $dom->getElementsByTagName('p')->item(0)->textContent;

            foreach($images_en as $k => $image){
                $data = $image->getAttribute('src');
                $image_path = explode('/', $data);
                $name_of_file = array_pop($image_path);

                if(!Storage::exists('images/services/'.$service->id.'/text_images/'.$name_of_file)){
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);
                    $fileName = uniqid().$k.'.jpg';
                    Storage::disk('public')->put('images/services/'.$service->id.'/text_images/'.$fileName, $data);
                    $image->removeAttribute('src');
                    $image->setAttribute('src', asset('storage/images/services/'.$service->id.'/text_images/'.$fileName));
                }
            }
            $text_en = $dom->saveHTML();
            /*===============================================================================================END UPDATE EN======*/

            $service->update([
                'active' => $visible,
                'name' => [
                    'es' => $request->input('name_es'),
                    'en' => $request->input('name_en')
                ],
                'description' => [
                    'es' => $request->input('description_es'),
                    'en' => $request->input('description_en')
                ],
                'text' => [
                    'es' => $text_es,
                    'en' => $text_en
                ],
                'short_text' => [
                    'es' => $short_text_es,
                    'en' => $short_text_en
                ]
            ]);


        } catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            session()->flash('message', ['type' => 'success', 'text'=>'Servicio actualizado correctamente']);
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
        }
        return back();

    }

}
