<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\ProductImage;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class ProductImageController extends Controller
{
    public function index(Product $product){
        $product->load([
            'product_images' => function($q){
                $q->orderBy('featured', 'DESC');
            }
        ])->get();
        return view('admin.products.images.index')->with(compact('product'));
    }

    public function store(Request $request, Product $product){
        $success = true;
        $files = $request->file('file');

        foreach($files as $file) {
            $fileName = uniqid()."_".$file->getClientOriginalName();
            $thumb_fileName = 'thumb_'.$fileName;
            if ($imagePath = $file->storeAs('images/products/'.$product->id.'/images', $fileName)) {
                $intervention = new ImageManager(['driver' => 'gd']);
                $image_thumb = $intervention->make(Storage::get($imagePath))->fit(450, 300)->encode();
                Storage::disk('public')->put('images/products/'.$product->id.'/thumbnails/' . $thumb_fileName, $image_thumb);
                //Storage::disk('public')->delete('images/' . $fileName);


                DB::beginTransaction();
                try {
                    $new_image = ProductImage::create([
                        'image' => $fileName,
                        'thumbnail' => $thumb_fileName,
                        'product_id' => $product->id
                    ]);
                } catch (\Exception $exception) {
                    $success = $exception->getMessage();
                    DB::rollBack();
                }
                if ($success === true) {
                    DB::commit();
                    if (count($product->images) == 1) {
                        $this->feature($product->id, $new_image->id);
                    }
                    session()->flash('message', ['type' => 'success', 'text'=>'imagen registrada correctamente'.$product->image_dir]);
                    /* $response = [
                         'type' => 'success',
                         'text' => 'imagenes registradas correctamente'
                     ];*/
                } else {
                    session()->flash('message', ['type' => 'danger', 'text'=>$success]);
                    /* $response = [
                         'type' => 'danger',
                         'text' => $success
                     ];*/
                }
            }
        }
        //return response()->json($response);
        return back();
    }

    public function destroy(ProductImage $product_image){

        $success = true;

        try{
            DB::beginTransaction();
            $product_image->delete();

            if ($product_image->featured == true && count($product_image->product->product_images)>0) {
                $product = $product_image->product;
                $image = $product->product_images->first();
                if ($image) {
                    $image->destacar($product->id);
                }
            }
        }
        catch (\Exception $exception){
            DB::rollBack();
            $success = $exception->getMessage();
        }
        if($success === true){

            DB::commit();
            session()->flash('message', ['type' => 'success', 'text'=>'Imagen eliminada correctamente']);
        }
        else{
            session()->flash('message', ['type' => 'success', 'text'=>$success]);
        }
        return back();

    }

    public function feature($product_id, $image_id){
        $product_image = ProductImage::find($image_id);
        $product_image->destacar($product_id);

        return back();
    }
}
