<?php

namespace App\Http\Controllers\Admin;

use App\FamilyProduct;
use App\Http\Requests\FamilyProductRequest;
use DebugBar\DebugBarException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class FamilyProductController extends Controller
{
    public function index(){
        $family_products = FamilyProduct::all();
        return view('admin.family_products.index')->with(compact('family_products'));
    }

    public function create(){
        return view('admin.family_products.create');
    }

    public function store(FamilyProductRequest $request){
        $success = true;
        DB::beginTransaction();
        try{
            $visible = true;
            if(!$request->has('visible')){
                $visible = false;
            }

            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_es'));
            $text_es = $dom->saveHTML();

            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_en'));
            $text_en = $dom->saveHTML();

            $family_product = FamilyProduct::create([
                'active' => $visible,
                'name' => [
                    'es' => $request->input('name_es'),
                    'en' => $request->input('name_en')
                ],
                'description' => [
                    'es' => $request->input('description_es'),
                    'en' => $request->input('description_en')
                ],
                'text' => [
                    'es' => $text_es,
                    'en' => $text_en
                ]
            ]);
            $fileName = null;
            if($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = uniqid() . $file->getClientOriginalName();
                if($imagePath = $file->storeAs('images', $fileName)){
                    $intervention = new ImageManager(['driver'=>'gd']);
                    $image = $intervention->make(Storage::get($imagePath))->fit(640, 480)->encode();
                    Storage::disk('public')->put('images/family_products/'.$family_product->id.'/featured/'.$fileName, $image);
                    Storage::disk('public')->delete('images/'.$fileName);
                    $family_product->update([
                       'image' => $fileName
                    ]);
                }
            }
        } catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            session()->flash('message', ['type' => 'success', 'text'=>'Familia de productos registrada correctamente']);
            return redirect(route('family_product.images'));
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
            return back();
        }

    }

    public function edit(FamilyProduct $family_product){
        return view('admin.family_products.edit')->with(compact('family_product'));
    }

    public function update(FamilyProductRequest $request, FamilyProduct $family_product){
        $success = true;
        DB::beginTransaction();
        try{
            Storage::disk('public')->delete('images/family_products/'.$family_product->id.'/featured/'.$family_product->image);
            $fileName = null;
            if($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = uniqid() . $file->getClientOriginalName();
                if($imagePath = $file->storeAs('images', $fileName)){
                    $intervention = new ImageManager(['driver'=>'gd']);
                    $image = $intervention->make(Storage::get($imagePath))->fit(640, 480)->encode();
                    Storage::disk('public')->put('images/family_products/'.$family_product->id.'/featured/'.$fileName, $image);
                    Storage::disk('public')->delete('images/'.$fileName);
                }
            }
            $visible = true;
            if(!$request->has('visible')){
                $visible = false;
            }

            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_es'));
            $text_es = $dom->saveHTML();

            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_en'));
            $text_en = $dom->saveHTML();

            $family_product->update([
                'image' => $fileName,
                'active' => $visible,
                'name' => [
                    'es' => $request->input('name_es'),
                    'en' => $request->input('name_en')
                ],
                'description' => [
                    'es' => $request->input('description_es'),
                    'en' => $request->input('description_en')
                ],
                'text' => [
                    'es' => $text_es,
                    'en' => $text_en
                ]
            ]);
        } catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            session()->flash('message', ['type' => 'success', 'text'=>'Familia de productos actualizada correctamente']);
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
        }
        return back();

    }


    public function destroy(FamilyProduct $family_product){
        $success = true;

        DB::beginTransaction();
        try{
            $family_product->delete();
        } catch(\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            //return response()->json(['type' => 'success', 'text' => 'Producto #'.$product->id.' eliminado correctamente']);
            session()->flash('message', ['type' => 'success', 'text'=>'Familia de productos eliminada correctamente']);
        }
        else{
            //return response()->json(['type' => 'warning', 'text' => $success]);
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
        }
        return back();
    }
}
