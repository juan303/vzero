<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\NewsRequest;
use App\News;
use DebugBar\DebugBarException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image;
use Intervention\Image\ImageManager;

class NewsController extends Controller
{
    public function index(){
        $news = News::all();
        return view('admin.news.index')->with(compact('news'));
    }

    public function create(){
        return view('admin.news.create');
    }

    public function store(NewsRequest $request){

        $success = true;
        DB::beginTransaction();
        try{
            $visible = true;
            if(!$request->has('visible')){
                $visible = false;
            }
            $new_new = News::create([
                'active' => $visible,
                'name' => [
                    'es' => $request->input('name_es'),
                    'en' => $request->input('name_en')
                ],
                'description' => [
                    'es' => $request->input('description_es'),
                    'en' => $request->input('description_en')
                ],
            ]);

            $fileName = null;
            if($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = uniqid() . $file->getClientOriginalName();
                if($imagePath = $file->storeAs('images', $fileName)){
                    $intervention = new ImageManager(['driver'=>'gd']);
                    $image = $intervention->make(Storage::get($imagePath))->fit(640, 480)->encode();
                    Storage::disk('public')->put('images/news/'.$new_new->id."/featured/".$fileName, $image);
                    Storage::disk('public')->delete('images/'.$fileName);
                }
            }

            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_es'));
            $images_es = $dom->getElementsByTagName('img');
            $short_text_es = $dom->getElementsByTagName('p')->item(0)->textContent;

            foreach($images_es as $k => $image){
                $data = $image->getAttribute('src');
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                $ImageFileName = uniqid().$k.'.jpg';
                Storage::disk('public')->put('images/news/'.$new_new->id.'/text_images/'.$ImageFileName, $data);
                $image->removeAttribute('src');
                $image->setAttribute('src', asset('storage/images/news/'.$new_new->id.'/text_images/'.$ImageFileName));
            }
            $text_es = $dom->saveHTML();


            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_en'));
            $images_en = $dom->getElementsByTagName('img');
            $short_text_en = $dom->getElementsByTagName('p')->item(0)->textContent;


            foreach($images_en as $k => $image){
                $data = $image->getAttribute('src');
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                $ImageFileName = uniqid().$k.'.jpg';
                Storage::disk('public')->put('images/news/'.$new_new->id.'/text_images/'.$ImageFileName, $data);
                $image->removeAttribute('src');
                $image->setAttribute('src', asset('storage/images/news/'.$new_new->id.'/text_images/'.$ImageFileName));
            }
            $text_en = $dom->saveHTML();

            $new_new->update([
                'image' => $fileName,//esto solo lo hago con la version en español
                'text' => [
                    'es' => $text_es,
                    'en' => $text_en
                ],
                'short_text' => [
                    'es' => $short_text_es,
                    'en' => $short_text_en
                ]
            ]);
        } catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            session()->flash('message', ['type' => 'success', 'text'=>'Noticia registrada correctamente']);
            return back();
            //return redirect(route('news.index'));
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
            return back();
        }

    }

    public function edit(News $news){
        $new = $news;
        return view('admin.news.edit')->with(compact('new'));
    }

    public function update(NewsRequest $request, News $news){
        $new = $news;
        $success = true;
        DB::beginTransaction();
        try{
            $visible = true;
            if(!$request->has('visible')){
                $visible = false;
            }

            $fileName = null;
            if($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = uniqid() . $file->getClientOriginalName();
                if($imagePath = $file->storeAs('images', $fileName)){
                    $intervention = new ImageManager(['driver'=>'gd']);
                    $image = $intervention->make(Storage::get($imagePath))->fit(640, 480)->encode();
                    Storage::disk('public')->put('images/news/'.$new->id."/featured/".$fileName, $image);
                    Storage::disk('public')->delete('images/'.$fileName);
                }
            }

            /*===================================================================================================UPDATE ES===*/
            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_es'));
            $images_es = $dom->getElementsByTagName('img');
            $short_text_es = $dom->getElementsByTagName('p')->item(0)->textContent;

            foreach($images_es as $k => $image){
                $data = $image->getAttribute('src');
                $image_path = explode('/', $data);
                $name_of_file = array_pop($image_path);

                if(!Storage::exists('images/news/'.$new->id.'/text_images/'.$name_of_file)){
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);
                    $fileName = uniqid().$k.'.jpg';
                    Storage::disk('public')->put('images/news/'.$new->id.'/'.$fileName, $data);
                    $image->removeAttribute('src');
                    $image->setAttribute('src', asset('storage/images/news/'.$new->id.'/'.$fileName));
                }
            }
            $text_es = $dom->saveHTML();
            /*===================================================================================================END UPDATE ES===*/

            /*===================================================================================================UPDATE EN===*/
            $dom = new \domdocument();
            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$request->input('text_en'));
            $images_en = $dom->getElementsByTagName('img');
            $short_text_en = $dom->getElementsByTagName('p')->item(0)->textContent;

            foreach($images_en as $k => $image){
                $data = $image->getAttribute('src');
                $image_path = explode('/', $data);
                $name_of_file = array_pop($image_path);

                if(!Storage::exists('images/news/'.$new->id.'/text_images/'.$name_of_file)){
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);
                    $fileName = uniqid().$k.'.jpg';
                    Storage::disk('public')->put('images/news/'.$new->id.'/'.$fileName, $data);
                    $image->removeAttribute('src');
                    $image->setAttribute('src', asset('storage/images/news/'.$new->id.'/'.$fileName));
                }
            }
            $text_en = $dom->saveHTML();
            /*===============================================================================================END UPDATE EN======*/

            $new->update([
                'image' => $fileName,
                'active' => $visible,
                'name' => [
                    'es' => $request->input('name_es'),
                    'en' => $request->input('name_en')
                ],
                'description' => [
                    'es' => $request->input('description_es'),
                    'en' => $request->input('description_en')
                ],
                'text' => [
                    'es' => $text_es,
                    'en' => $text_en
                ],
                'short_text' => [
                    'es' => $short_text_es,
                    'en' => $short_text_en
                ]
            ]);


        } catch (\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            session()->flash('message', ['type' => 'success', 'text'=>'Noticia actualizada correctamente']);
        }
        else{
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
        }
        return back();

    }


    public function destroy(News $news){
        $new = $news;
        $success = true;

        DB::beginTransaction();
        try{
            $new->delete();
        } catch(\Exception $exception){
            $success = $exception->getMessage();
            DB::rollBack();
        }
        if($success === true){
            DB::commit();
            //return response()->json(['type' => 'success', 'text' => 'Producto #'.$product->id.' eliminado correctamente']);
            session()->flash('message', ['type' => 'success', 'text'=>'Noticia eliminada correctamente']);
        }
        else{
            //return response()->json(['type' => 'warning', 'text' => $success]);
            session()->flash('message', ['type' => 'danger', 'text'=>$success]);
        }
        return back();
    }
}
