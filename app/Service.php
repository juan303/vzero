<?php

namespace App;

use igaster\TranslateEloquent\TranslationTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Service extends Model
{
    use TranslationTrait;
    protected static $translatable = ['name', 'text', 'description'];

    protected $fillable = ['image', 'active', 'name', 'text', 'description'];

    protected static function boot(){
        parent::boot();
        static::deleting(function(Service $service){
            Storage::disk('public')->deleteDirectory('images/news/'.$service->id);
        });
    }

    public function make_url($service_id){
        return 'storage/images/services/'.$service_id.'/'.$this->image;
    }

    //SCOPES
    public function scopeActive($query, $flag){
        return $query->where('active', $flag);
    }
}
