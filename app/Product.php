<?php

namespace App;

use igaster\TranslateEloquent\TranslationTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{

    use TranslationTrait;

    protected static $translatable = ['name', 'text', 'description'];

    protected $fillable = ['description', 'name', 'image', 'active', 'family_product_id'];

    protected static function boot()
    {
        parent::boot();
        static::deleting(function (Product $product) {
            Storage::disk('public')->deleteDirectory('images/products/'.$product->id);
        });
    }

    public function business_areas(){
        return $this->belongsToMany(BusinessArea::class);
    }

    public function family_product(){
        return $this->belongsTo(FamilyProduct::class);
    }

    public function product_images(){
        return $this->hasMany(ProductImage::class);
    }

    public function getFeaturedImageAttribute(){

        return $this->product_images->where('featured', true)->first();
    }

    public function has_business_area($business_area_id){
        foreach($this->business_areas as $business_area){
            if($business_area->id == $business_area_id){
                return true;
            }
        }
        return false;
    }

    public function getImageDirAttribute(){
        return $this->id;
    }

    //SCOPES
    public function scopeActive($query, $flag){
        return $query->where('active', $flag);
    }

}
