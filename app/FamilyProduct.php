<?php

namespace App;

use igaster\TranslateEloquent\TranslationTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class FamilyProduct extends Model
{

    use TranslationTrait;

    protected static $translatable = ['name', 'text', 'description'];

    protected $fillable = ['text','description','name','image','active'];

    protected static function boot(){
        parent::boot();
        static::deleting(function(BusinessArea $business_area){
            Storage::disk('public')->delete('images/family_products/'.$business_area->image);
        });
    }

    public function products(){
        return $this->hasMany(Product::class);
    }

    public function make_image_url($family_product_id){
        if($this->image == NULL ){
            return 'storage/images/default/default.jpg';
        }
        return 'storage/images/family_products/'.$family_product_id.'/featured/'.$this->image;
    }


    //accessors
    public function getUrlImageAttribute(){

        if($this->image == NULL){
            return 'storage/images/family_products/default.png';
        }
        return 'storage/images/family_products/'.$this->image;
    }

    //SCOPES
    public function scopeActive($query, $flag){
        return $query->where('active', $flag);
    }
}
