<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ProductImage extends Model
{
    protected $fillable = ['product_id', 'image', 'thumbnail', 'featured', 'name', 'description'];

    protected static function boot()
    {
        parent::boot();
        static::deleting(function (ProductImage $image) {
            Storage::disk('public')->delete('images/products/'.$image->product->image_dir."/images/".$image->image);
            Storage::disk('public')->delete('images/products/'.$image->product->image_dir."/thumbnails/".$image->thumbnail);
        });
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function destacar($product_id){
        ProductImage::where('product_id', $product_id)->update([
            'featured' => false,
        ]);
        $this->featured = true;
        $this->save();
    }

    public function make_url($product_id){
        if($this->image == NULL){
            return 'storage/images/default/default.jpg';
        }
        return 'storage/images/products/'.$product_id.'/images/'.$this->image;
    }

    public function make_thumbnail_url($product_id){
        if($this->image == NULL){
            return 'storage/images/default/default.jpg';
        }
        return 'storage/images/products/'.$product_id.'/thumbnails/'.$this->thumbnail;
    }
}
