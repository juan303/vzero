<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessAreasTranslation extends Model
{
    use SoftDeletes;

    protected $fillable = ['locale', 'name', 'text', 'description', 'business_area_id'];

    public function business_area(){
        return $this->belongsTo(BusinessArea::class);
    }
}



