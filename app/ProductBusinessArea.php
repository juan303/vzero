<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBusinessArea extends Model
{
    protected $fillable = ['product_id', 'business_area_id'];

    public function business_area(){
        return $this->belongsTo(BusinessArea::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
