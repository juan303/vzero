<?php

namespace App;

use igaster\TranslateEloquent\TranslationTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class BusinessArea extends Model
{
    use TranslationTrait;
    protected static $translatable = ['name', 'text', 'description'];

    protected $fillable = ['image', 'active', 'name', 'text', 'description'];

    protected static function boot(){
        parent::boot();
        static::deleting(function(BusinessArea $business_area){
            Storage::disk('public')->deleteDirectory('images/business_areas/'.$business_area->id);
        });
    }

    public function products(){
        return $this->hasMany(Product::class);
    }


    public function make_image_url($business_area_id){
        if($this->image == NULL ){
            return 'storage/images/default/default.jpg';
        }
        return 'storage/images/business_areas/'.$business_area_id.'/featured/'.$this->image;
    }


    //SCOPES
    public function scopeActive($query, $flag){
        return $query->where('active', $flag);
    }

}
