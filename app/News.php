<?php

namespace App;

use igaster\TranslateEloquent\TranslationTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class News extends Model
{
    use TranslationTrait;

    protected static $translatable = ['name', 'text', 'short_text', 'description'];

    protected $fillable = ['description', 'name', 'image', 'active', 'text', 'short_text'];

    protected static function boot(){
        parent::boot();
        static::deleting(function(News $news){
            Storage::disk('public')->deleteDirectory('images/news/'.$news->id);
        });
    }

    //SCOPES
    public function scopeActive($query, $flag){
        return $query->where('active', $flag);
    }


    public function make_url($new_id){

        return 'storage/images/news/'.$new_id.'/featured/'.$this->image;
    }
}
