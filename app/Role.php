<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ADMIN = 1;
    const USER = 2;
    const LEVEL1 = 3;

    public function users(){
        return $this->hasMany(User::class);
    }
}
