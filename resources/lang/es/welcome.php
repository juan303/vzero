<?php
return [
    'Formulario de contacto'       => 'Formulario de contacto',
    'Enviar mensaje' => 'Enviar mensaje',
    'Tu nombre' => 'Tu nombre',
    'Asunto' => 'Asunto',
    'Empresa o universidad' => 'Empresa o universidad',
    'Tu mensaje' => 'Tu mensaje',
    'Tu email' => 'Tu email'
];
