<?php
return [
    'Panel de control' => 'Panel de control',
    'Idioma'       => 'Idioma',
    'Productos'       => 'Productos',
    'Servicios' => 'Servicios',
    'Noticias' => 'Noticias',
    'Últimas noticias' => 'Últimas noticias',
    'Leer más' => 'Leer más',
    'Todas las noticias' => 'Todas las noticias',
    'Áreas de negocio' => 'Áreas de negocio',
    'Contacto' => 'Contacto',
    'Socios' => 'Socios',
    'Familias de productos' => 'Familias de productos'
];
