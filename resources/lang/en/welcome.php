<?php
return [
    //===========================Formulario de contacto
    'Formulario de contacto'       => 'Contact form',
    'Enviar mensaje' => 'Send message',
    'Tu nombre' => 'Your name',
    'Asunto' => 'Subject',
    'Empresa o universidad' => 'Enterprise or University',
    'Tu mensaje' => 'Your message',
    'Tu email' => 'Your email'
];
