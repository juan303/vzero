<?php
return [
    'Panel de control' => 'Control panel',
    'Idioma'       => 'Language',
    'Productos'       => 'Products',
    'Áreas de negocio' => 'Business areas',
    'Servicios' => 'Services',
    'Noticias' => 'News',
    'Últimas noticias' => 'Latest news',
    'Leer más' => 'Read more',
    'Todas las noticias' => 'All news',
    'Contacto' => 'Contact',
    'Socios' => 'Partners',
    'Familias de productos' => 'Family products'
];
