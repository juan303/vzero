@extends('layouts.app')

@section('body-class', 'sidebar-collapse')

@section('navigation')
    @include('partials.navigations.navigation')
@endsection

@section('styles')
    <style>
        .main-section{
            margin-top: 100px;
            margin-bottom: 25px;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $(".article img").addClass('rounded');
            $(".article img.note-float-left").addClass('mr-3 mb-1');
            $(".article img.note-float-right").addClass('ml-3 mb-1');
        })
    </script>
@endsection

@section('content')
    <div class="main main-section pb-5">
        <div class="container pt-2">
            <div class="row">
                <div class="col">
                    <h2 class="title text-dark mb-1">{{ $product->name }}</h2>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-lg-8 col-md-8 ml-auto mr-auto article">
                    <article>
                        {!! $product->text !!}
                    </article>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        @forelse($product->product_images as $image)
                            <div class="col-6">
                                <img class="img-fluid rounded mb-3" src="{{ asset($image->make_url($product->id)) }}" alt="">
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="row justify-content-center mt-3">
                <a href="{{ route('family_products.index', ['family_product' => $product->family_product->id]) }}" class="btn btn-vzero">{{ __('buttons.Volver') }}</a>
            </div>
        </div>
    </div>

    @include('partials.messages.message')
@endsection
