@if(auth()->user())
    <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link text-success dropdown-toggle"  href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ auth()->user()->name }} <span class="caret"></span>
        </a>
        <div class="dropdown-menu" v-bind:aria-labelledby="navbarDropdown">
        @if(auth()->user()->email_verified_at)
            @if(auth()->user()->role->name === 'admin')
                <a href="{{ route('dashboard') }}" target="_blank" class="dropdown-item">
                    {{ __('Panel de control') }}
                </a>
                <hr />
            @else
            @endif

            <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                {{ __('Cerrar sesión') }}
            </a>
            <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
                @csrf
            </form>

        @else

        @endif
        </div>
    </li>

@else
    <li class="nav-item">
        <a href="{{ route('login') }}" class="nav-link text-info"><i class="fa fa-user"></i> Usuarios</a>
    </li>

@endif






