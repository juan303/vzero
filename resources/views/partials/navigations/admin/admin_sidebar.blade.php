<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo">
        <a href="{{ Route('welcome') }}" class="simple-text logo-normal">
            V-Zero
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('dashboard') }}">
                    <i class="material-icons">dashboard</i>
                    <p>{{ __('navigation.Panel de control') }}</p>
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="{{ route('business_area.index') }}">
                    <i class="material-icons">person</i>
                    <p>{{ __('navigation.Áreas de negocio') }}</p>
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="{{ route('family_product.index') }}">
                    <i class="material-icons">content_paste</i>
                    <p>{{ __('navigation.Familias de productos') }}</p>
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="{{ route('product.index') }}">
                    <i class="material-icons">library_books</i>
                    <p>{{ __('navigation.Productos') }}</p>
                </a>
            </li>
           <li class="nav-item ">
                <a class="nav-link" href="{{ route('news.index') }}">
                    <i class="material-icons">bubble_chart</i>
                    <p>{{ __('navigation.Noticias') }}</p>
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="{{ route('service.index') }}">
                    <i class="material-icons">location_ons</i>
                    <p>{{ __('navigation.Servicios') }}</p>
                </a>
            </li>
            {{--<li class="nav-item ">
                <a class="nav-link" href="./notifications.html">
                    <i class="material-icons">notifications</i>
                    <p>Notifications</p>
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="./rtl.html">
                    <i class="material-icons">language</i>
                    <p>RTL Support</p>
                </a>
            </li>
            <li class="nav-item active-pro ">
                <a class="nav-link" href="./upgrade.html">
                    <i class="material-icons">unarchive</i>
                    <p>Upgrade to PRO</p>
                </a>
            </li>--}}
        </ul>
    </div>
</div>