<li class="nav-item dropdown">
    <div class="dropdown">
        <a id="navbarDropdownLang" class="nav-link text-success dropdown-toggle"  href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ session()->get('locale') }}
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownLang">
            <a class="dropdown-item" href="{{ url('locale/en') }}" >EN</a>
            <a class="dropdown-item" href="{{ url('locale/es') }}" >ES</a>
        </div>
    </div>
</li>


