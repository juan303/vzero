
{{--<nav class="@yield('nav-class')" @yield('transparency')>--}}
<nav class="nav-class navbar navbar-expand-lg bg-white fixed-top" >
    <div class="container">
        <div class="navbar-translate">
            <a class="navbar-brand pt-0" href="{{ route('welcome') }}">
                <img style="width: 100px" src="{{ asset('images/logo.png') }}" alt="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
                @include('partials.navigations.languages')
                <li class="nav-item">
                    <a class="nav-link" data-scroll href="#areas_de_negocio">{{ __('navigation.Áreas de negocio') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#productos">{{ __('navigation.Productos') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#servicios">{{ __('navigation.Servicios') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#noticias">{{ __('navigation.Noticias') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">{{ __('Socios') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">{{ __('Careers') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('contact') }}">{{ __('navigation.Contacto') }}</a>
                </li>
                @include('partials.navigations.users')
            </ul>
        </div>
    </div>
</nav>