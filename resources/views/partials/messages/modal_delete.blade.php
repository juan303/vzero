<div class="modal fade" id="confirm_{{ $item_id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form action="{{ $url }}" method="post">
        @csrf
        @method('DELETE')
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">¡Atencion!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <p>{{ $message }}</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-warning w-25">{{ __('SI') }}</button>
                    <button type="button" data-dismiss="modal" class="btn btn-primary w-25">{{ __('NO') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>