@if(session('message'))
<div class="col-md-12">
    <div class="alert alert-{{ session('message')['type'] }} alert-dismissible fade show" role="alert">
        <div class="container">
            <div class="alert-icon">
                @if(session('message')['type'] == 'danger')
                    <i class="far fa-thumbs-down"></i>
                @else
                    <i class="far fa-thumbs-up"></i>
                @endif
            </div>
            {{ session('message')['text'] }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">
                <span aria-hidden="true">&times;</span>
            </span>
            </button>
        </div>
    </div>
</div>
@endif
