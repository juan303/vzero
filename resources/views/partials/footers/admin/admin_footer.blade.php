<footer class="footer">
    <div class="container-fluid">
        <nav class="float-left">
            <ul>
                <li>
                    <a href="{{ route('welcome') }}">
                        V-Zero
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright float-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script> <i class="material-icons">favorite</i>
            <a href="{{ route('dashboard') }}">V-Zero control panel</a>
        </div>
    </div>
</footer>