@extends('layouts.app')

@section('body-class', 'sidebar-collapse')

@section('navigation')
    @include('partials.navigations.navigation')
@endsection

@section('styles')
    <style>
        .main-section{
            margin-top: 100px;
            margin-bottom: 25px;
        }
        html {
            min-height: 100%;
            position: relative;
        }
        body {
            margin: 0;
        //margin-bottom: 50px;
        }
        footer {
        //background-color: black;
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 82px;
            color: white;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $(".article img").addClass('rounded');
            $(".article img").each(function(){
                if($(this).hasClass('note-float-left')){
                    $(this).addClass('mr-3');
                }
                if($(this).hasClass('note-float-right')){
                    $(this).addClass('ml-3');
                }
            })
        })
    </script>
@endsection

@section('content')
    <div class="main main-section pb-5">
        <div class="container pt-2">
            <div class="row">
                <div class="col ml-auto mr-auto article">
                    <h2 class="title text-dark">{{ $family_product->name }}</h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <p>{!! $family_product->text !!}</p>
                </div>
                <div class="col-md-6">
                    @forelse($family_product->products as $product)
                        <div class="col-md-6">
                            <div class="card">
                                <a href="{{ route('product.show', ['product' => $product->id]) }}">
                                    <img class="card-img-top" src="{{ asset($product->featured_image->make_thumbnail_url($product->id)) }}" alt="Card image cap">
                                </a>
                                <div class="card-body">
                                    <a href="{{ route('product.show', ['product' => $product->id]) }}"><h4 class="card-title">{{ $product->name }}</h4></a>
                                </div>
                            </div>
                        </div>
                    @empty
                    @endforelse
                </div>
            </div>
            <div class="row justify-content-center mt-3">
                <a href="{{ route('welcome') }}" class="btn btn-vzero">{{ __('buttons.Volver') }}</a>
            </div>
        </div>
    </div>

    @include('partials.messages.message')
@endsection
