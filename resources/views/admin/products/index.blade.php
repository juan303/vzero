@extends('layouts.admin')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @include('partials.messages.general_messages')
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('product.create') }}" class="btn btn-success">{{ __('Agregar productos') }}</a>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">{{ __('navigation.Productos') }}</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            {{ __('Nombre') }}
                                        </th>
                                        <th>
                                            {{ __('Creado') }}
                                        </th>
                                        <th>
                                            {{ __('Activo') }}
                                        </th>
                                        <th>
                                            {{ __('Acciones') }}
                                        </th>
                                    </thead>
                                    <tbody>
                                        @forelse($products as $product)
                                            <tr>
                                                <td>
                                                    {{ $product->id }}
                                                </td>
                                                <td>
                                                    {{ $product->name }}
                                                </td>
                                                <td>
                                                    {{ $product->created_at->format('d/m/Y') }}
                                                </td>
                                                <td>
                                                    {{ $product->active }}
                                                </td>
                                                <td>
                                                    <a href="{{ route('product.edit', ['product' => $product->id]) }}"  rel="tooltip" title="{{ __('Editar') }}" class="btn btn-link px-1 text-success px-0 my-0 py-0">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ route ('product_images.index', ['product'=>$product->id]) }}" rel="tooltip" title="Editar imagenes" class="btn btn-link px-1 text-warning px-0 my-0 py-0">
                                                        <i class="fa fa-image"></i>
                                                    </a>
                                                    <button type="button" data-toggle="modal" data-target="#confirm_{{ $product->id }}" rel="tooltip" title="¿{{ __('Eliminar') }}?" class="btn btn-delete px-1 btn-link text-danger my-0 py-0">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                    @include('partials.messages.modal_delete', ['url' => route('product.destroy', ['product'=>$product->id]),
                                                        'item_id' => $product->id,
                                                        'message' => '¿Seguro que quiere eliminar "'.$product->name.'"?'])
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="4"><div class="alert alert-info">{{ __('Ningún productos definido') }}</div></td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection