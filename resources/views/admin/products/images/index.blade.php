@extends('layouts.admin')

@section('scripts')
    <script type="text/javascript">
        Dropzone.autoDiscover = true;
        Dropzone.options.mydropzone = {
            addRemoveLinks: true,
            autoProcessQueue: true,
            dictRemoveFile: "Eliminar",
            uploadMultiple: true,
            parallelUploads: 3,
            maxFilesize: 10,
            maxFiles: null,
            init: function(file) {
                var dropz = this;
                this.on("queuecomplete", function(data) {
                    location.reload()
                });
                $('#procesar_fotos').click(function () {
                    dropz.processQueue();
                })
            }
        }
    </script>
@endsection


@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <h3>Imagenes para "{{ $product->name }}"</h3>
            </div>
            @include('partials.messages.general_messages')
            <div class="row mb-3">
                <div class="col-md-12">
                    <form id="mydropzone" name="file[]" type="file" multiple class="dropzone"  action="{{ route ('product_images.store', ['product'=>$product->id]) }}"  method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="dz-message" data-dz-message><span>Arrastra aquí tus imágenes</span></div>
                        <div class="row justify-content-center">
                            <button id="procesar_fotos" type="button" class="btn btn-warning btn-lg">Procesar fotos</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                @foreach($product->product_images as $image)
                    <div class="col-lg-3 col-md-6">
                        <div class="card  @if($image->featured == true) border-primary border @endif">
                            <img class="card-img-top" src="{{ asset($image->make_thumbnail_url($product->id)) }}" alt="Card image cap">
                            <div class="card-body">
                                <form action="{{ route('product_images.destroy', ['product_image' => $image->id]) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button href="#" class="btn btn-danger">{{ __('Eliminar') }}</button>
                                    @if($image->featured != true)
                                        <a href="{{ route('product_images.feature', ['product_id' => $product->id, 'image_id' => $image->id]) }}" class="btn btn-success btn-just-icon" rel="tooltip" title="Destacar imagen">
                                            <i class="now-ui-icons ui-2_favourite-28"></i>
                                        </a>
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ route ('product.index') }}" class="btn btn-warning btn-lg">Volver</a>
                </div>
            </div>
        </div>
    </div>
@endsection