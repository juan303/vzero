@extends('layouts.admin')

@section('scripts')
    @include('admin.scripts.summernote_config_fotos')
@endsection

@section('content')

    <div class="content">
        <div class="container-fluid">
            @include('partials.messages.general_messages')
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <form action="{{ route('product.update', ['product' => $product->id]) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="card-header card-header-primary">
                                <h4 class="card-title ">{{ __('Editar producto') }}: "{{ $product->name }}"</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <h4 class="mb-4">Castellano</h4>
                                        <div class="form-group">
                                            <label for="name_es">{{ __('Nombre') }}</label>
                                            <input type="text" name="name_es" class="form-control" id="name_es" value="{{ old('name_es', $product->translate('es')->name) }}">
                                            @if ($errors->has('name_es'))
                                                <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('name_es') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="description_es">{{ __('Descripción') }}</label>
                                            <input id="description_es" type="text" name="description_es" class="form-control" value="{{ old('description_es', $product->translate('es')->description) }}" >
                                            @if ($errors->has('description_es'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('description_es') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="text_es">{{ __('Texto') }}</label>
                                            <textarea id="text_es" type="text" name="text_es" class="summernote form-control"  >{!!old ('text_es', $product->translate('es')->text) !!}</textarea>
                                            @if ($errors->has('text_es'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('text_es') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h4 class="mb-4">Inglés</h4>
                                        <div class="form-group">
                                            <label for="name_en">{{ __('Nombre') }}</label>
                                            <input type="text" name="name_en" class="form-control" id="name_en" value="{{ old('name_en', $product->translate('en')->name) }}">
                                            @if ($errors->has('name_en'))
                                                <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('name_en') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="description_en">{{ __('Descripción') }}</label>
                                            <input id="description_en" type="text" name="description_en" class="form-control" value="{{ old('description_en', $product->translate('en')->description) }}" >
                                            @if ($errors->has('description_en'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('description_en') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="text_en">{{ __('Texto') }}</label>
                                            <textarea id="text_en" type="text" name="text_en" class="summernote form-control" >{!!old ('text_en', $product->translate('en')->text) !!}</textarea>
                                            @if ($errors->has('text_en'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('text_en') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="family_product" class="mr-auto">{{ __('Familia de productos') }}</label>
                                            <select name="family_product" id="family_product" class="custom-select">
                                                @foreach($family_products as $family_product)
                                                    <option @if($product->family_product->id == $family_product->id) selected @endif value="{{ $family_product->id }}">{{ $family_product->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="image" class="mr-auto d-block">{{ __('Áreas de negocios') }}</label>
                                            @foreach($business_areas as $business_area)
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" name="business_areas[]" @if(is_array(old('business_areas')) && in_array($business_area->id, old('business_areas'))) checked @elseif($product->has_business_area($business_area->id)) checked @endif id="inlineCheckbox1" value="{{ $business_area->id }}">{{ $business_area->name }}
                                                        <span class="form-check-sign">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input checked name="visible" class="form-check-input" type="checkbox" >
                                                {{ __('Visible') }}
                                                <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer justify-content-start">
                                <a href="{{ route('product.index') }}" class="btn btn-warning">Volver</a>
                                <button type="submit"  class="btn btn-default">Guardar cambios</button>
                                <a href="{{ route('product_images.index', ['product' => $product->id]) }}" class="btn btn-warning">Editar imágenes</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection