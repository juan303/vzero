@extends('layouts.admin')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @include('partials.messages.general_messages')
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('business_area.create') }}" class="btn btn-success">{{ __('Agregar área de negocio') }}</a>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">{{ __('navigation.Áreas de negocio') }}</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            {{ __('Nombre') }}
                                        </th>
                                        <th>
                                            {{ __('Creado') }}
                                        </th>
                                        <th>
                                            {{ __('Activa') }}
                                        </th>
                                        <th>
                                            {{ __('Acciones') }}
                                        </th>
                                    </thead>
                                    <tbody>
                                        @forelse($business_areas as $business_area)
                                            <tr>
                                                <td>
                                                    {{ $business_area->id }}
                                                </td>
                                                <td>
                                                    {{ $business_area->name }}
                                                </td>
                                                <td>
                                                    {{ $business_area->created_at->format('d/m/Y') }}
                                                </td>
                                                <td>
                                                    {{ $business_area->active }}
                                                </td>
                                                <td>
                                                    <a href="{{ route('business_area.edit', ['business_area' => $business_area->id]) }}"  rel="tooltip" title="{{ __('Editar') }}" class="btn btn-link px-1 text-success px-0 my-0 py-0">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                   {{-- <a href="{{ route ('images.index', ['product'=>$product->id]) }}" rel="tooltip" title="Editar imagenes" class="btn btn-link px-1 text-warning px-0 my-0 py-0">
                                                        <i class="fa fa-image"></i>
                                                    </a>--}}
                                                    <button type="button" data-toggle="modal" data-target="#confirm_{{ $business_area->id }}" rel="tooltip" title="¿{{ __('Eliminar') }}?" class="btn btn-delete px-1 btn-link text-danger my-0 py-0">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                    @include('partials.messages.modal_delete', ['url' => route('business_area.destroy', ['business_area'=>$business_area->id]),
                                                        'item_id' => $business_area->id,
                                                        'message' => '¿Seguro que quiere eliminar "'.$business_area->name.'"?'])
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="4"><div class="alert alert-info">{{ __('Ningún área de negocio definida') }}</div></td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection