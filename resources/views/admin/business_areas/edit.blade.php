@extends('layouts.admin')

@section('scripts')
    @include('admin.scripts.summernote_config_fotos')
@endsection

@section('content')

    <div class="content">
        <div class="container-fluid">
            @include('partials.messages.general_messages')
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <form action="{{ route('business_area.update', ['business_area' => $business_area->id]) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="card-header card-header-primary">
                                <h4 class="card-title ">{{ __('Editar área de negocio') }}: {{ $business_area->name }}</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <h4 class="mb-4">Castellano</h4>
                                        <div class="form-group">
                                            <label for="name_es">{{ __('Nombre') }}</label>
                                            <input type="text" name="name_es" class="form-control" id="name_es" value="{{ old('name_es', $business_area->translate('es')->name) }}">
                                            @if ($errors->has('name_es'))
                                                <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('name_es') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="description_es">{{ __('Descripción') }}</label>
                                            <input id="description_es" type="text" name="description_es" class="form-control" value="{{ old('description_es', $business_area->translate('es')->description) }}" >
                                            @if ($errors->has('description_es'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('description_es') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="text_es">{{ __('Texto') }}</label>
                                            <textarea id="text_es" type="text" name="text_es" class="summernote form-control"  >{!!old ('text_es', $business_area->translate('es')->text) !!}</textarea>
                                            @if ($errors->has('text_es'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('text_es') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h4 class="mb-4">Inglés</h4>
                                        <div class="form-group">
                                            <label for="name_en">{{ __('Nombre') }}</label>
                                            <input type="text" name="name_en" class="form-control" id="name_en" value="{{ old('name_en', $business_area->translate('en')->name) }}">
                                            @if ($errors->has('name_en'))
                                                <span class="text-danger" role="alert">
                                                <strong>{{ $errors->first('name_en') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="description_en">{{ __('Descripción') }}</label>
                                            <input id="description_en" type="text" name="description_en" class="form-control" value="{{ old('description_en', $business_area->translate('en')->description) }}" >
                                            @if ($errors->has('description_en'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('description_en') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="text_en">{{ __('Texto') }}</label>
                                            <textarea id="text_en" type="text" name="text_en" class="summernote form-control" >{!!old ('text_en', $business_area->translate('en')->text) !!}</textarea>
                                            @if ($errors->has('text_en'))
                                                <span class="text-danger" role="alert">
                                                    <strong>{{ $errors->first('text_en') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form row">
                                    <div class="col-md-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input @if($business_area->active) checked @endif name="visible" class="form-check-input" type="checkbox" >
                                                {{ __('Visible') }}
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="image" class="mr-auto">{{ __('Imagen de la miniatura') }}</label>
                                                    <div class="input-group">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" id="image" name="image">
                                                            <label class="custom-file-label" for="image">Buscar imagen</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <img class="img-fluid img-thumbnail" src="{{ asset($business_area->make_image_url($business_area->id)) }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer justify-content-start">
                                <a href="{{ route('business_area.index') }}" class="btn btn-warning">Volver</a>
                                <button type="submit"  class="btn btn-default">{{ __('Actualizar') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection