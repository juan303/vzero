@extends('layouts.admin')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @include('partials.messages.general_messages')
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('service.create') }}" class="btn btn-success">{{ __('Agregar servicio') }}</a>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">{{ __('navigation.Servicios') }}</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            {{ __('Nombre') }}
                                        </th>
                                        <th>
                                            {{ __('Creado') }}
                                        </th>
                                        <th>
                                            {{ __('Activo') }}
                                        </th>
                                        <th>
                                            {{ __('Acciones') }}
                                        </th>
                                    </thead>
                                    <tbody>
                                        @forelse($services as $service)
                                            <tr>
                                                <td>
                                                    {{ $service->id }}
                                                </td>
                                                <td>
                                                    {{ $service->name }}
                                                </td>
                                                <td>
                                                    {{ $service->created_at->format('d/m/Y') }}
                                                </td>
                                                <td>
                                                    {{ $service->active }}
                                                </td>
                                                <td>
                                                    <a href="{{ route('service.edit', ['service' => $service->id]) }}"  rel="tooltip" title="{{ __('Editar') }}" class="btn btn-link px-1 text-success px-0 my-0 py-0">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    {{--<a href="{{ route ('product_images.index', ['product'=>$product->id]) }}" rel="tooltip" title="Editar imagenes" class="btn btn-link px-1 text-warning px-0 my-0 py-0">
                                                        <i class="fa fa-image"></i>
                                                    </a>--}}
                                                    <button type="button" data-toggle="modal" data-target="#confirm_{{ $service->id }}" rel="tooltip" title="¿{{ __('Eliminar') }}?" class="btn btn-delete px-1 btn-link text-danger my-0 py-0">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                    @include('partials.messages.modal_delete', ['url' => route('service.destroy', ['service'=>$service->id]),
                                                        'item_id' => $service->id,
                                                        'message' => '¿Seguro que quiere eliminar "'.$service->name.'"?'])
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="4"><div class="alert alert-info">{{ __('Ningún servicio definido') }}</div></td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection