@extends('layouts.admin')

@section('content')

    <div class="content">
        <div class="container-fluid">
            @include('partials.messages.general_messages')
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('family_product.create') }}" class="btn btn-success">{{ __('Agregar familia de productos') }}</a>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">{{ __('navigation.Familias de productos') }}</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                        <th>
                                            ID
                                        </th>
                                        <th>
                                            {{ __('Nombre') }}
                                        </th>
                                        <th>
                                            {{ __('Creado') }}
                                        </th>
                                        <th>
                                            {{ __('Activa') }}
                                        </th>
                                        <th>
                                            {{ __('Acciones') }}
                                        </th>
                                    </thead>
                                    <tbody>
                                        @forelse($family_products as $family_product)
                                            <tr>
                                                <td>
                                                    {{ $family_product->id }}
                                                </td>
                                                <td>
                                                    {{ $family_product->name }}
                                                </td>
                                                <td>
                                                    {{ $family_product->created_at->format('d/m/Y') }}
                                                </td>
                                                <td>
                                                    {{ $family_product->active }}
                                                </td>
                                                <td>
                                                    <a href="{{ route('family_product.edit', ['family_product' => $family_product->id]) }}"  rel="tooltip" title="{{ __('Editar') }}" class="btn btn-link px-1 text-success px-0 my-0 py-0">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                   {{-- <a href="{{ route ('images.images', ['product'=>$product->id]) }}" rel="tooltip" title="Editar imagenes" class="btn btn-link px-1 text-warning px-0 my-0 py-0">
                                                        <i class="fa fa-image"></i>
                                                    </a>--}}
                                                    <button type="button" data-toggle="modal" data-target="#confirm_{{ $family_product->id }}" rel="tooltip" title="¿{{ __('Eliminar') }}?" class="btn btn-delete px-1 btn-link text-danger my-0 py-0">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                    @include('partials.messages.modal_delete', ['url' => route('family_product.destroy', ['family_product'=>$family_product->id]),
                                                        'item_id' => $family_product->id,
                                                        'message' => '¿Seguro que quiere eliminar "'.$family_product->name.'"?'])
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="4"><div class="alert alert-info">{{ __('Ningúna familia de productos definida') }}</div></td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection