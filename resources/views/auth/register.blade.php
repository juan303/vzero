@extends('layouts.app')

@section('body-class', 'login-page sidebar-collapse')

@section('navigation')
    @include('partials.navigations.navigation')
@endsection

@section('styles')
    <style>
        html {
            min-height: 100%;
            position: relative;
        }
        body {
            margin: 0;
            //margin-bottom: 50px;
        }
        footer {
            //background-color: black;
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 82px;
            color: white;
        }
        .login-page .page-header > .container{
            padding-bottom:65px;
        }
    </style>
@endsection

@section('content')
    <div class="page-header header-filter" style="background-image: url('{{ asset('img/bg7.jpg') }}'); background-size: cover; background-position: top center;">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-10 ml-auto mr-auto">
                    <div class="card card-login">
                        <form class="form" method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="card-header card-header-primary text-center">
                                <h4 class="card-title">Registro</h4>
                            </div>
                            <div class="card-body px-3 py-2">
                                <div class="form-row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="name" class="col-form-label text-md-right">{{ __('Nombre') }}</label>
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                            @if ($errors->has('name'))
                                                <span class="ml-5 invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="surname" class="col-form-label text-md-right">{{ __('Apellidos') }}</label>
                                            <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ old('surname') }}"  autocomplete="surname" >
                                            @if ($errors->has('surname'))
                                                <span class="ml-5 invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('surname') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-form-label text-md-right">{{ __('E-Mail') }}</label>
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                            @if ($errors->has('email'))
                                                <span class="ml-5 invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-form-label text-md-right">{{ __('Contraseña') }}</label>
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                            @if ($errors->has('password'))
                                                <span class="ml-5 invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="password-confirm" class="col-form-label text-md-right">{{ __('Confirmar Contraseña') }}</label>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

                                            @if ($errors->has('password_confirmation'))
                                                <span class="ml-5 invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">

                                        <div class="form-group">
                                            <label for="enterprise" class="col-form-label text-md-right">{{ __('Empresa') }} / {{ __('Universidad') }}</label>
                                            <input id="enterprise" type="text" class="form-control @error('enterprise') is-invalid @enderror" name="enterprise" value="{{ old('enterprise') }}"  autocomplete="enterprise" >
                                            @error('enterprise')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="department" class="col-form-label text-md-right">{{ __('Departamento') }} / {{ __('Área') }}</label>
                                            <input id="department" type="text" class="form-control @error('department') is-invalid @enderror" name="department" value="{{ old('department') }}"  autocomplete="department" >
                                            @error('department')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="dni" class="col-form-label text-md-right">{{ __('DNI') }} / {{ __('NIF') }}</label>
                                            <input id="dni" type="text" class="form-control @error('dni') is-invalid @enderror" name="dni" value="{{ old('dni') }}"  autocomplete="dni">

                                            @error('dni')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="tel" class="col-form-label text-md-right">{{ __('Teléfono de contacto') }}</label>
                                            <input id="tel" type="text" class="form-control @error('tel') is-invalid @enderror" name="tel" value="{{ old('tel') }}"  autocomplete="tel">
                                            @error('tel')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                       {{-- <div class="form-group">
                                            <label for="mov" class="col-form-label text-md-right">{{ __('Teléfono móvil') }}</label>
                                            <input id="mov" type="text" class="form-control @error('mov') is-invalid @enderror" name="mov" value="{{ old('mov') }}"  autocomplete="mov">
                                            @error('mov')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>--}}

                                        <div class="form-group">
                                            <label for="country" class="col-form-label text-md-right">{{ __('País') }}</label>
                                                <input id="country" type="text" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ old('country') }}"  autocomplete="country">
                                                @error('country')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                </div>
                                <div class="form-row justify-content-center">
                                    <div class="form-group mb-0">
                                        <button type="submit" class="btn btn-vzero">
                                            {{ __('Registrar') }}
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
