<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>{{ env('APP_NAME') }}</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <!-- Favicons -->
        <link href="img/favicon.png" rel="icon">
        <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

        <!-- Bootstrap CSS File -->


        <!-- Libraries CSS Files -->
        {{--<link href="{{ asset('lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">--}}


        <link href="{{ asset('lib/animate/animate.min.css') }}" rel="stylesheet">
       {{-- <link href="{{ asset('lib/venobox/venobox.css') }}" rel="stylesheet">
        <link href="{{ asset('lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">--}}



        <!-- Main Stylesheet File -->
        {{--<link href="{{ asset('css/style.css') }}" rel="stylesheet">--}}


        <link href="{{ asset('css/material-kit.css?v=2.0.5') }}" rel="stylesheet" />
        <link href="{{ asset('css/demo.css') }}" rel="stylesheet">
        <link href="{{ asset('css/mi_css.css') }}" rel="stylesheet">
        @yield('styles')
        <!-- =======================================================
          Theme Name: TheEvent
          Theme URL: https://bootstrapmade.com/theevent-conference-event-bootstrap-template/
          Author: BootstrapMade.com
          License: https://bootstrapmade.com/license/
        ======================================================= -->
    </head>

    <body class="@yield('body-class')" data-spy="scroll">
        @yield('navigation')
        <div class="wrapper">
            @yield('content')
        </div>
        @include('partials.footers.footer')


        <!-- JavaScript Libraries -->
        <script src="{{ asset('js/core/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/core/popper.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/plugins/moment.min.js') }}"></script>
        <script src="{{ asset('js/material-kit.js?v=2.0.5') }}"></script>
        <script src="{{ asset('js/smooth-scroll.min.js') }}"></script>



       {{-- <script src="{{ asset('lib/easing/easing.min.js') }}"></script>
        <script src="{{ asset('lib/superfish/hoverIntent.js') }}"></script>
        <script src="{{ asset('lib/superfish/superfish.min.js') }}"></script>
        <script src="{{ asset('lib/wow/wow.min.js') }}"></script>
        <script src="{{ asset('lib/venobox/venobox.min.js') }}"></script>
        <script src="{{ asset('lib/owlcarousel/owl.carousel.min.js') }}"></script>--}}

        <!-- Contact Form JavaScript File -->
        {{--<script src="{{ asset('contactform/contactform.js') }}"></script>--}}

        <!-- Template Main Javascript File -->
        {{--<script src="{{ asset('js/main.js') }}"></script>--}}
        @yield('scripts')


    </body>
</html>