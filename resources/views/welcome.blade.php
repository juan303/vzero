@extends('layouts.app')

@section('navigation')
    @include('partials.navigations.navigation_welcome')
@endsection

@section('scripts')
    <script src="{{ asset('js/smooth-scroll.min.js') }}"></script>
    <script>
        var scroll = new SmoothScroll('a[href*="#"]', {
            speed: 600
        });
        $(document).ready(function () {
            $('#contact-form').submit(function (e) {
                e.preventDefault();
                var url = $(this).attr('action');
                $('.form-error').css('display', 'none');
                $.post(url, $(this).serialize(), function(data){
                    $("#modal_message .alert").attr('class', 'alert alert-success');
                    $('#modal_message .alert').html(data.text);
                    $('#modal_message').modal('show')
                }).fail(function(data){
                    var json = JSON.stringify(data);
                    var json_array = $.parseJSON(json);
                    $.each(json_array.responseJSON.errors, function(key, value){
                        $('#'+key+'_error').css('display', 'block');
                        $('#'+key+'_error').html(value);
                    });
                })
            })
        })
    </script>
@endsection


@section('styles')
    <style>
        @media (max-width: 991px) {
            .navbar, .navbar-collapse {
                overflow: auto;
                max-height: 85vh;
                align-items: flex-start;
            }
        }
        #noticias .card .card-header {
            margin-top: -13px;
        }
    </style>
@endsection





@section('content')
    <div class="page-header header-filter" data-parallax="true" style="background-image: url('{{ asset('images/fondo_1.jpg') }}')">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="title">SOLUCIONES <span class="text-info">DE INGENIERIA</span></h2>
                    <h4>Every landing page needs a small description after the big bold title, that&apos;s why we added this text here. Add here all the information that can make you or your product create the first impression.</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="main main-section">
        <div class="container">
            <div class="section text-center" id="areas_de_negocio">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto">
                        <h2 class="title mb-2">{{ __('navigation.Áreas de negocio') }}</h2>
                        {{--<h5 class="description">This is the paragraph where you can write more details about your product. Keep you user engaged by providing meaningful information. Remember that by this time, the user is curious, otherwise he wouldn&apos;t scroll to get here. Add a button if you want the user to see more.</h5>--}}
                    </div>
                </div>
                <div class="features">
                    <div class="row">
                        @forelse($business_areas as $business_area)
                            <div class="col-md-4 col-sm-6 col-6">
                                <div class="team-player">
                                    <div class="card card-plain mb-0">
                                        <div class="col-md-12 ml-auto mr-auto">
                                            <a href="{{ route('business_area.show', ['$business_area'=>$business_area->id]) }}">
                                                <img class="img-fluid rounded" src="{{ asset($business_area->make_image_url($business_area->id)) }}" alt="{{ $business_area->name."_".$loop->iteration }}">
                                            </a>
                                        </div>
                                        <h4 class="card-title">{{ $business_area->name }}
                                            <br>
                                        </h4>
                                        <div class="card-body">
                                            <p class="card-description">{{ $business_area->description }}</p>
                                        </div>
                                        {{--<div class="card-footer justify-content-center">
                                            <a href="#pablo" class="btn btn-link btn-just-icon"><i class="fa fa-twitter"></i></a>
                                            <a href="#pablo" class="btn btn-link btn-just-icon"><i class="fa fa-instagram"></i></a>
                                            <a href="#pablo" class="btn btn-link btn-just-icon"><i class="fa fa-facebook-square"></i></a>
                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-section main" style="background-color: #e9f3f7 ">
        <div class="container">
            <div class="section text-center" id="productos">
                <h2 class="title">{{ __('navigation.Productos') }}</h2>
                <div class="team">
                    <div class="row">
                        @forelse($family_products as $family_product)
                        <div class="col-md-4 col-sm-6">
                            <div class="team-player">
                                <div class="card card-plain mb-0">
                                    <div class="col-md-10 ml-auto mr-auto">
                                        <a href="{{ route('family_products.index', ['family_product' => $family_product->id]) }}"><img src="{{ $family_product->url_image }}" alt="{{ $family_product->name }}_{{ $loop->iteration }}" class=" rounded img-fluid"></a>
                                    </div>
                                    <h4 class="card-title">{{ $family_product->name }}
                                        <br>
                                    </h4>
                                    <div class="card-body">
                                        <p class="card-description">{{ $family_product->description }}</p>
                                    </div>
                                    {{--<div class="card-footer justify-content-center">
                                        <a href="#pablo" class="btn btn-link btn-just-icon"><i class="fa fa-twitter"></i></a>
                                        <a href="#pablo" class="btn btn-link btn-just-icon"><i class="fa fa-instagram"></i></a>
                                        <a href="#pablo" class="btn btn-link btn-just-icon"><i class="fa fa-facebook-square"></i></a>
                                    </div>--}}
                                </div>
                            </div>
                        </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-section main" >
        <div class="container">
            <div class="section" id="servicios">
                <h2 class="title text-center">{{ __('navigation.Servicios') }}</h2>
                <div class="team">
                    <div class="row">
                        @forelse($services as $service)
                            <div class="col-md-4 col-sm-6">
                                <div class="team-player">
                                    <div class="card card-plain mb-0">

                                            <img src="{{ $service->make_url($service->id) }}" alt="{{ $service->name }}_{{ $loop->iteration }}" class=" rounded img-fluid">

                                        <h4 class="card-title">{{ $service->name }}
                                            <br>
                                        </h4>
                                        <div class="card-body">
                                            <p class="card-description">{{ $service->description }}</p>
                                        </div>
                                        {{--<div class="card-footer justify-content-center">
                                            <a href="#pablo" class="btn btn-link btn-just-icon"><i class="fa fa-twitter"></i></a>
                                            <a href="#pablo" class="btn btn-link btn-just-icon"><i class="fa fa-instagram"></i></a>
                                            <a href="#pablo" class="btn btn-link btn-just-icon"><i class="fa fa-facebook-square"></i></a>
                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-section main" style="background-color: #e9f3f7 ">
        <div class="container">
            <div class="section" id="noticias">
                <h2 class="title text-center">{{ __('navigation.Últimas noticias') }}</h2>
                <div class="team">
                    <div class="row">
                        @forelse($news as $new)
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header card-header-primary py-1">
                                    <span class="font-italic text-light">{{ $new->created_at->format('d/m/Y') }}</span>
                                </div>
                                <div class="card-body pb-1">
                                    <h5 class="card-title">{{ $new->name }}</h5>
                                    <img src="{{ asset($new->make_url($new->id)) }}" class="img-fluid w-50 float-left mr-4" alt="">
                                    <p class="card-text">
                                        {{ $new->short_text }}...
                                    </p>
                                    <p class="text-right"><a href="#" class="btn btn-vzero">{{ __('navigation.Leer más') }}</a></p>
                                </div>
                            </div>
                        </div>
                        @empty
                        @endforelse
                    </div>
                    <div class="row">
                        <div class="col-12 text-right">
                            <button class="btn btn-default">{{ __('navigation.Todas las noticias') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection




