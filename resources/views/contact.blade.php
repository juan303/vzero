@extends('layouts.app')

@section('body-class', 'login-page sidebar-collapse')

@section('navigation')
    @include('partials.navigations.navigation')
@endsection

@section('styles')
    <style>
        html {
            min-height: 100%;
            position: relative;
        }
        body {
            margin: 0;
            margin-bottom: 50px;
        }
        footer {
        //background-color: black;
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 82px;
            color: white;
        }
        .login-page .page-header > .container{
            padding-bottom: 65px;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('js/smooth-scroll.min.js') }}"></script>
    <script>
        var scroll = new SmoothScroll('a[href*="#"]', {
            speed: 600
        });
        $(document).ready(function () {
            $('#contact-form').submit(function (e) {
                e.preventDefault();
                var url = $(this).attr('action');
                $('.form-error').css('display', 'none');
                $.post(url, $(this).serialize(), function(data){
                    $("#modal_message .alert").attr('class', 'alert alert-success');
                    $('#modal_message .alert').html(data.text);
                    $('#modal_message').modal('show')
                }).fail(function(data){
                    var json = JSON.stringify(data);
                    var json_array = $.parseJSON(json);
                    $.each(json_array.responseJSON.errors, function(key, value){
                        $('#'+key+'_error').css('display', 'block');
                        $('#'+key+'_error').html(value);
                    });
                })
            })
        })
    </script>
@endsection

@section('content')
    <div class="page-header header-filter" style="background-image: url('{{ asset('img/bg7.jpg') }}'); background-size: cover; background-position: top center;">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 ml-auto mr-auto">
                    @include('partials.forms.contact_form')
                </div>
            </div>
        </div>
    </div>
@endsection
