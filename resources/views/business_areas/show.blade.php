@extends('layouts.app')

@section('body-class', 'sidebar-collapse')

@section('navigation')
    @include('partials.navigations.navigation')
@endsection

@section('styles')
    <style>
        .main-section{
            margin-top: 100px;
            margin-bottom: 25px;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $(".article img").addClass('rounded');
            $(".article img").each(function(){
                if($(this).hasClass('note-float-left')){
                    $(this).addClass('mr-3');
                }
                if($(this).hasClass('note-float-right')){
                    $(this).addClass('ml-3');
                }
            })
        })
    </script>
@endsection

@section('content')
        <div class="main main-section pb-5">
            <div class="container pt-5">
                <div class="row">
                    <div class="col-lg-10 col-md-10 ml-auto mr-auto article">
                        <h2 class="title text-dark">{{ $business_area->name }}</h2>
                        <hr>

                        <article>
                            {!! $business_area->text !!}
                        </article>
                    </div>
                </div>
                <div class="row justify-content-center mt-3">
                    <a href="{{ route('welcome') }}" class="btn btn-vzero">{{ __('buttons.Volver') }}</a>
                </div>
            </div>
        </div>

    @include('partials.messages.message')
@endsection
