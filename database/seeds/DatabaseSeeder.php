<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Role::class, 1)->create(['name' => 'admin']);
        factory(Role::class, 1)->create(['name' => 'user']);
        factory(Role::class, 1)->create(['name' => 'level1']);

        factory(User::class, 1)->create([
            'name' => 'Juan',
            'username' => 'bob2501303',
            'email' => 'gdf000@hotmail.com',
            'password' => bcrypt('secret'),
            'active' => true,
            'role_id' => Role::ADMIN
        ]);

    }
}
