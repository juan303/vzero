<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilyProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_products', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('name')->unsigned()->nullable();
            $table->integer('text')->unsigned()->nullable();
            $table->integer('description')->unsigned()->nullable();

            $table->string('image')->nullable();
            $table->boolean('active')->default(true);

            $table->timestamps();
        });

        Schema::create('business_areas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('image')->nullable();
            $table->boolean('active')->default(true);

            $table->integer('name')->unsigned()->nullable();
            $table->integer('text')->unsigned()->nullable();
            $table->integer('description')->unsigned()->nullable();

            $table->timestamps();
        });


        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('image')->nullable();
            $table->boolean('active')->default(true);

            $table->integer('name')->unsigned()->nullable();
            $table->integer('text')->unsigned()->nullable();
            $table->integer('description')->unsigned()->nullable();

            //FK
            $table->bigInteger('family_product_id')->unsigned();
            $table->foreign('family_product_id')->references('id')->on('family_products');


            $table->timestamps();
        });

        Schema::create('business_area_product', function (Blueprint $table) {
            $table->bigIncrements('id');

            //FK
            $table->bigInteger('business_area_id')->unsigned();
            $table->foreign('business_area_id')->references('id')->on('business_areas')->onDelete('cascade');

            $table->bigInteger('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_products');
        Schema::dropIfExists('business_areas');
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_business_areas');
    }
}
