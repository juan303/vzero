<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\FamilyProduct;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'text' => $faker->text(200),
        'description' => $faker->sentence(10),
        'active' => true,

        'family_product_id' => FamilyProduct::all()->random()->id
    ];
});

