<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\FamilyProduct;
use Faker\Generator as Faker;

$factory->define(FamilyProduct::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'text' => $faker->text(200),
        'description' => $faker->sentence(10),
        'active' => true,

        'family_product_id' => FamilyProduct::all()->random()->id
    ];
});
