<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Role;
use Faker\Generator as Faker;

$factory->define(Role::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement([Role::ADMIN, Role::LEVEL1, Role::USER]),
        'description' => $faker->sentence,
    ];
});
