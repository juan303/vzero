<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});


Route::group(['middleware' => 'localization'], function(){
    Route::get('/', 'WelcomeController@index')->name('welcome');
});


Auth::routes();
Auth::routes(['verify' => true]);





Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth', 'admin'], 'namespace' => 'Admin'], function(){
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    Route::resource('business_area','BusinessAreaController');
    Route::resource('family_product','FamilyProductController');

    Route::resource('product','ProductController');
    Route::group(['prefix' => 'products/images'], function(){
        Route::get('/{product}/index', 'ProductImageController@index')->name('product_images.index'); //listar imagenes
        Route::post('/{product}/store', 'ProductImageController@store')->name('product_images.store'); //almacenar imagenes
        Route::delete('/{product_image}/destroy', 'ProductImageController@destroy')->name('product_images.destroy'); //eliminar imagen
        Route::get('/{product_id}/{image_id}', 'ProductImageController@feature')->name('product_images.feature'); //destacar imagen
    });

    Route::resource('news','NewsController');
    Route::resource('service','ServiceController');

});

//======Contacto
Route::post('message', 'ContactMessageController@send')->name('contact_message.send');
Route::get('contact', function(){
    return view('contact');
})->name('contact');

//======Areas de negocios
Route::get('business_areas/{business_area}', 'BusinessAreaController@show')->name('business_area.show');

//======Familias de productos
Route::get('family_products/{family_product}', 'FamilyProductController@index')->name('family_products.index');

//======Productos
Route::get('products/{product}', 'ProductController@show')->name('product.show');

